﻿namespace Wolfram
{
    partial class AutoInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.locationLabel = new System.Windows.Forms.Label();
            this.refreshButton = new System.Windows.Forms.Button();
            this.weatherForecastPicture = new System.Windows.Forms.PictureBox();
            this.currentLocInfoPicture = new System.Windows.Forms.PictureBox();
            this.weatherLabel = new System.Windows.Forms.Label();
            this.currentLocFactsLabel = new System.Windows.Forms.Label();
            this.daysUntilXmasPicture = new System.Windows.Forms.PictureBox();
            this.daysUntilXmasLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.weatherForecastPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.currentLocInfoPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.daysUntilXmasPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // locationLabel
            // 
            this.locationLabel.AutoSize = true;
            this.locationLabel.Location = new System.Drawing.Point(13, 13);
            this.locationLabel.Name = "locationLabel";
            this.locationLabel.Size = new System.Drawing.Size(136, 13);
            this.locationLabel.TabIndex = 0;
            this.locationLabel.Text = "Assumed Location = ?????";
            // 
            // refreshButton
            // 
            this.refreshButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.refreshButton.Location = new System.Drawing.Point(432, 353);
            this.refreshButton.Name = "refreshButton";
            this.refreshButton.Size = new System.Drawing.Size(75, 23);
            this.refreshButton.TabIndex = 1;
            this.refreshButton.Text = "Refresh Info";
            this.refreshButton.UseVisualStyleBackColor = true;
            this.refreshButton.Click += new System.EventHandler(this.refreshButton_Click);
            // 
            // weatherForecastPicture
            // 
            this.weatherForecastPicture.Location = new System.Drawing.Point(16, 57);
            this.weatherForecastPicture.Name = "weatherForecastPicture";
            this.weatherForecastPicture.Size = new System.Drawing.Size(185, 100);
            this.weatherForecastPicture.TabIndex = 2;
            this.weatherForecastPicture.TabStop = false;
            // 
            // currentLocInfoPicture
            // 
            this.currentLocInfoPicture.Location = new System.Drawing.Point(322, 57);
            this.currentLocInfoPicture.Name = "currentLocInfoPicture";
            this.currentLocInfoPicture.Size = new System.Drawing.Size(185, 100);
            this.currentLocInfoPicture.TabIndex = 3;
            this.currentLocInfoPicture.TabStop = false;
            // 
            // weatherLabel
            // 
            this.weatherLabel.AutoSize = true;
            this.weatherLabel.Location = new System.Drawing.Point(13, 41);
            this.weatherLabel.Name = "weatherLabel";
            this.weatherLabel.Size = new System.Drawing.Size(92, 13);
            this.weatherLabel.TabIndex = 4;
            this.weatherLabel.Text = "Weather Forecast";
            // 
            // currentLocFactsLabel
            // 
            this.currentLocFactsLabel.AutoSize = true;
            this.currentLocFactsLabel.Location = new System.Drawing.Point(319, 41);
            this.currentLocFactsLabel.Name = "currentLocFactsLabel";
            this.currentLocFactsLabel.Size = new System.Drawing.Size(88, 13);
            this.currentLocFactsLabel.TabIndex = 5;
            this.currentLocFactsLabel.Text = "Location Fact-file";
            // 
            // daysUntilXmasPicture
            // 
            this.daysUntilXmasPicture.Location = new System.Drawing.Point(16, 195);
            this.daysUntilXmasPicture.Name = "daysUntilXmasPicture";
            this.daysUntilXmasPicture.Size = new System.Drawing.Size(133, 27);
            this.daysUntilXmasPicture.TabIndex = 6;
            this.daysUntilXmasPicture.TabStop = false;
            // 
            // daysUntilXmasLabel
            // 
            this.daysUntilXmasLabel.AutoSize = true;
            this.daysUntilXmasLabel.Location = new System.Drawing.Point(13, 179);
            this.daysUntilXmasLabel.Name = "daysUntilXmasLabel";
            this.daysUntilXmasLabel.Size = new System.Drawing.Size(104, 13);
            this.daysUntilXmasLabel.TabIndex = 7;
            this.daysUntilXmasLabel.Text = "Days until Christmas:";
            // 
            // AutoInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(519, 388);
            this.Controls.Add(this.daysUntilXmasLabel);
            this.Controls.Add(this.daysUntilXmasPicture);
            this.Controls.Add(this.currentLocFactsLabel);
            this.Controls.Add(this.weatherLabel);
            this.Controls.Add(this.currentLocInfoPicture);
            this.Controls.Add(this.weatherForecastPicture);
            this.Controls.Add(this.refreshButton);
            this.Controls.Add(this.locationLabel);
            this.Name = "AutoInfo";
            this.Text = "AutoInfo";
            ((System.ComponentModel.ISupportInitialize)(this.weatherForecastPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.currentLocInfoPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.daysUntilXmasPicture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label locationLabel;
        private System.Windows.Forms.Button refreshButton;
        private System.Windows.Forms.PictureBox weatherForecastPicture;
        private System.Windows.Forms.PictureBox currentLocInfoPicture;
        private System.Windows.Forms.Label weatherLabel;
        private System.Windows.Forms.Label currentLocFactsLabel;
        private System.Windows.Forms.PictureBox daysUntilXmasPicture;
        private System.Windows.Forms.Label daysUntilXmasLabel;
    }
}