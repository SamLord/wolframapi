﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Wolfram
{
    public partial class AutoInfo : Form
    {
        Wolfram autoWolf = new Wolfram();


        public AutoInfo(string assumedLoc, string appID)
        {
            InitializeComponent();
            autoWolf.AppID = appID;
            autoWolf.Location = assumedLoc;
            locationLabel.Text = "Assumed location = " + autoWolf.Location;
        }

        private void UpdateInfo()
        {
            string[] imageSources = autoWolf.GetLocalInfo();

            if (imageSources != null)
            {
                if (imageSources[0] != "")
                {
                    weatherForecastPicture.Load(imageSources[0]);
                }
                if (imageSources[1] != "")
                {
                    currentLocInfoPicture.Load(imageSources[1]);
                }
            }
            
        }

        private void refreshButton_Click(object sender, EventArgs e)
        {
            UpdateInfo();
        }
    }
}
