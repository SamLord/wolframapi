﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Wolfram
{
    public partial class Dialogue : Form
    {
        public Dialogue(string displayText)
        {
            InitializeComponent();
            displayTextBox.Text = displayText;
        }
    }
}
