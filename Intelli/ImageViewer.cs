﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Wolfram
{
    public partial class ImageViewer : Form
    {
        int index = 0;
        string[] images;

        public ImageViewer(string[] imgUrls)
        {
            InitializeComponent();
            largePictureBox.Load(imgUrls[index]);
            images = imgUrls;
            DoButtonCheck();
        }

        private void leftButton_Click(object sender, EventArgs e)
        {
            if (index > 0)
            {
                index -= 1;
                loadImage();
            }
            DoButtonCheck();
        }

        private void rightButton_Click(object sender, EventArgs e)
        {
            if (index < images.Length-1)
            {
                index += 1;
                loadImage();
            }
            DoButtonCheck();
        }

        private void loadImage()
        {
            largePictureBox.ImageLocation = images[index];
        }

        private void DoButtonCheck()
        {
            if (index == 0)
            {
                leftButton.Enabled = false;
            }
            else
            {
                leftButton.Enabled = true;
            }
            if (images[index+1] == null)
            {
                rightButton.Enabled = false;
            }
            else
            {
                rightButton.Enabled = true;
            }
        }
    }
}
