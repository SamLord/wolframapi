﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace Wolfram
{
    public partial class IntelliHomeForm : Form
    {

        private Wolfram wolfram = new Wolfram();
        String information;
        string settingsFile = "IntelliSet.conf";

        public IntelliHomeForm()
        {
            InitializeComponent();
            SetUpWolfram();

            if (File.Exists(settingsFile))
            {
                LoadSettings();
            }
            else
            {
                File.CreateText(settingsFile);
            }
        }

        private void SubmitButton_Click(object sender, EventArgs e)
        {
            SubmitButton.Text = "Working...";
            SubmitButton.Enabled = false;
            queryWorker.RunWorkerCompleted += queryWorker_RunWorkerCompleted;
            queryWorker.RunWorkerAsync();
        }

        private void SetUpWolfram()
        {
            wolfram.AppID = "7K53PP-WEWWLAWKTX";
        }

        private void lastRecievedRawToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (wolfram.LastReceived != null)
            {
                Form displayData = new Dialogue(wolfram.LastReceived);
                displayData.Show();
            }
            else
            {
                Form displayData = new Dialogue("NO DATA");
                displayData.Show();
            }
        }

        private void largerImageButton_Click(object sender, EventArgs e)
        {
            Form imageViewForm = new ImageViewer(wolfram.ImageURLs);
            imageViewForm.Show();
            
        }

        public void LoadSettings()
        {
            string fileContent = File.ReadAllText(settingsFile);

            foreach (string setting in fileContent.Split(';'))
            {
                if (setting != "" && setting != null)
                {
                    string[] splitSetting = setting.Split('=');

                    if (splitSetting[0].ToLower() == "location")
                    {
                        wolfram.Location = splitSetting[1];
                    }
                }
            }
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (Form settingsForm = new Settings())
            {
                settingsForm.ShowDialog(this);
            }
        }

        private void autoInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {

            AutoInfo infoScreen = new AutoInfo(wolfram.Location, wolfram.AppID);
            infoScreen.Show();
        }

        private void queryWorker_RunWorkerCompleted(
            object sender,
            RunWorkerCompletedEventArgs e)
        {

            SubmitButton.Text = "Submit";
            SubmitButton.Enabled = true;
            if (OutputTextBox.Text == "")
            {
                largerImageButton.Enabled = false;
            }

            if (wolfram.ImageURLs != null)
            {
                largerImageButton.Enabled = true;
            }

            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message);
            }
            else
            {
                OutputTextBox.Text = information;
            }
        }

        private void queryWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            largerImageButton.Enabled = false;

            if (InputTextBox.Text != null)
            {
                wolfram.Query(InputTextBox.Text);
                information = wolfram.GetParsedResponse();
            }
        }
    }
}
