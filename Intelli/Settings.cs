﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Wolfram
{
    public partial class Settings : Form
    {
        string settingFile = "IntelliSet.conf";

        public Settings()
        {
            InitializeComponent();
        }

        private void saveChangesButton_Click(object sender, EventArgs e)
        {
            string toSave = "Location=" + locationTextBox.Text + Environment.NewLine;

            if (File.Exists(settingFile))
            {
                File.WriteAllText(settingFile, toSave);
            }
            else
            {
                try
                {
                    File.Create(settingFile);
                    File.WriteAllText(settingFile, toSave);
                }
                catch
                {
                    MessageBox.Show("An error occured saving settings, please try again.","Error Occured",MessageBoxButtons.OK,MessageBoxIcon.Error);
                }
            }

            IntelliHomeForm parent = (IntelliHomeForm)this.Owner;
            parent.LoadSettings();

        }
    }
}
