﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Net;
using System.Xml;
using System.IO;

namespace Wolfram
{
    class Wolfram //last request variable (just store returnedValue in RawQuery and Query (maybe use a private method for that))
    {
        //http://api.wolframalpha.com/v2/query?input=pi&appid=XXXX
        private string appID;
        private string lastReceivedString;
        private XmlDocument lastReceivedXmlDoc;
        private string[] imageURLs = new string[20];
        private string location;

        public Wolfram()
        {
            appID = "XXX";
            location = "London, UK";
        }

        public Wolfram(string ID, string loc)
        {
            appID = ID;
            location = loc;
        }

        public string Location
        {
            get
            {
                return location;
            }
            set
            {
                location = value;
            }
        }

        public string[] ImageURLs
        {
            get
            {
                return imageURLs;
            }
        }

        public string AppID
        {
            get
            {
                return appID;
            }
            set
            {
                appID = value;
            }
        }

        public string LastReceived
        {
            get
            {
                return lastReceivedString;
            }
        }

        public XmlDocument LastReceivedXmlDoc
        {
            get
            {
                return lastReceivedXmlDoc;
            }
        }

        public string RawQuery(string queryString)
        {
            string returnedValue = "";
            string getURL;

            if (appID != "XXX")
            {
                getURL = "http://api.wolframalpha.com/v2/query?" + "&appid=" + appID + "&input=" + queryString + "&format=image,plaintext" + "&location=" + location;
                returnedValue = GetResponseAsString(getURL);
            }
            else
            {
                Debug.WriteLine("AppID not set!");
            }
            lastReceivedString = returnedValue;
            return returnedValue;
        }

        public XmlDocument Query(string queryString)
        {
            string getURL;
            XmlDocument receivedDoc;
            imageURLs = null;

            if (appID != "XXX")
            {
                getURL = "http://api.wolframalpha.com/v2/query?" + "&appid=" + appID + "&input=" + queryString + "&format=image,plaintext" + "&location=" + location;
                lastReceivedXmlDoc = GetResponseAsXmlDoc(getURL);
                receivedDoc = lastReceivedXmlDoc;
            }
            else
            {
                Debug.WriteLine("AppID not set!");
                return null;
            }

            return receivedDoc;
        }

        private String GetResponseAsString(string requestURL)
        {
            string received;

            WebClient client = new WebClient();
            received = client.DownloadString(requestURL);

            return received;
        }

        private XmlDocument GetResponseAsXmlDoc(string requestURL)
        {
            XmlDocument xmlDoc = new XmlDocument();

            HttpWebRequest request = WebRequest.Create(requestURL) as HttpWebRequest;
            HttpWebResponse response = request.GetResponse() as HttpWebResponse;

            xmlDoc.Load(response.GetResponseStream());
            lastReceivedString = xmlDoc.OuterXml;
            return xmlDoc;
        }

        public string GetParsedResponse()
        {
            string parsedString = "";


            XmlNamespaceManager xmlnsMngr = new XmlNamespaceManager(lastReceivedXmlDoc.NameTable);

            XmlNodeList podElements = lastReceivedXmlDoc.SelectNodes("queryresult/pod", xmlnsMngr);
            XmlNodeList imgElements = lastReceivedXmlDoc.SelectNodes("queryresult/pod/subpod/img", xmlnsMngr);

            foreach (XmlNode node in podElements)
            {
                parsedString += node.Attributes["title"].Value + Environment.NewLine;
                parsedString += node.InnerText + Environment.NewLine;
                parsedString += Environment.NewLine;
            }

            imageURLs = new string[20];
            try
            {
                int i = 0;

                foreach (XmlNode node in imgElements)
                {
                    imageURLs[i] = node.Attributes["src"].Value;
                    i += 1;
                }

                //XmlNode imgNode;
                //while (imgNode != null)
                //{
                //    imgNode = imgElements[0;
                //    imageURLs[0] = imgNode.Attributes["src"].Value;
                //    i += 1;
                //    imgNode = imgNode.NextSibling;
                //}


                //imageURL = lastReceivedXmlDoc.SelectSingleNode("queryresult/pod/subpod/img", xmlnsMngr).Attributes["src"].Value;
            }
            catch
            {
                imageURLs = null;
            }

            return parsedString;
        }

        public string GetRawResponse()
        {
            return lastReceivedString;
        }

        public string[] GetLocalInfo()
        {
            string[] infoImages = new string[2];

            Query("weather here");
            infoImages[0] = GetAtrribValue(lastReceivedXmlDoc, "title", "weather forecast", "src", "queryresult/pod/subpod/img");

            Query(location);
            infoImages[1] = GetAtrribValue(lastReceivedXmlDoc, "title", "city population", "src", "queryresult/pod/subpod/img");

            Query(location);

            return infoImages;
        }

        public string GetAtrribValue(XmlDocument doc, string checkedAttribName, string checkedAttribValueMustContain, string returnedAttribName, string nodeSelect)
        {
            string matchedString = "";

            XmlNamespaceManager xmlnsMngr = new XmlNamespaceManager(doc.NameTable);

            XmlNodeList elements = doc.SelectNodes(nodeSelect, xmlnsMngr);

            foreach (XmlNode node in elements)
            {
                if (node.Attributes[checkedAttribName].Value.Contains(checkedAttribValueMustContain))
                {
                    matchedString = node.Attributes[returnedAttribName].Value;
                    break;
                }
            }

            return matchedString;
        }

    }
}
